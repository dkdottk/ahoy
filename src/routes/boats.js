const getManufacturerStoredData = require("../services/getManufacturerStoredData");
const getConfigurationStoredData = require("../services/getConfigurationStoredData");
const express = require('express')
let app = express.Router()

app.get('/boats/data', (req, res) => {
  getManufacturerStoredData().then(function(data) {
      return res.status(200).json(data);
  });
});

app.get('/boats/configurations', (req, res) => {
  getConfigurationStoredData().then(function() {
      return res.status(200).json(data);
  });
});

module.exports = app;
