require('dotenv').config();
const express = require('express');
const axios = require('axios');

const app = express();

const routes = require('./routes/index');
app.route(routes);

const config = require('./config');
const caching = require('./cron/caching');

const getManufacturerStoredData = require("./services/getManufacturerStoredData");
const getConfigurationStoredData = require("./services/getConfigurationStoredData");

axios.defaults.headers.common = {
    "X-API-Key": config.price_digest_api_key,
};

// app.get('/boats/data', (req, res) => {
//     getManufacturerStoredData().then(function(data) {
//         return res.status(200).json(data);
//     });
// });

// app.get('/boats/configurations', (req, res) => {
//     getConfigurationStoredData().then(function() {
//         return res.status(200).json(data);
//     });
// });

caching();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.listen(config.web.port, function (err) {
        if (err)
            throw err;
        console.log('Server running in http://127.0.0.1:3000');
    });

module.exports = app;
