const redis = require('redis');

const client = redis.createClient();

module.exports = async () => {
    client.hgetall('configurationsList', function (err, reply) {
        return data = JSON.parse(reply.data);
    });
    return data;
};