const axios = require('axios');
const config = require("../config")
const BASE_URL = "https://pricedigestsapi.com";

module.exports = async () => {
    try {
        return await axios.get(`${BASE_URL}/v1/taxonomy/configurations/?year=${config.fake_data.year}&manufacturerId=${config.fake_data.manufacturerId}`);
    } catch (error) {
        console.error(error);
    }
};
