const redis = require('redis');
const client = redis.createClient();
const getConfigurationListFromApi = require("./getConfigurationList");

module.exports = async () => {
    try {
        let response = await getConfigurationListFromApi();
        let filteredData = response.data.map(function (item) {
            return { modelName: item.modelName, configurationId: item.configurationId };
        });
        
        client.hset('configurationsList', 'data', JSON.stringify(filteredData));

    } catch (error) {
        console.error(error);
    }
}