const axios = require('axios');
const config = require("../config")
const BASE_URL = "https://pricedigestsapi.com";

module.exports = async () => {
    try {
        return await axios.get(`${BASE_URL}/v1/taxonomy/manufacturers/?classificationId=${config.fake_data.classificationId}`);
    } catch (error) {
        console.error(error);
    };
};
