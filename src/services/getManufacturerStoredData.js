const redis = require('redis');

const client = redis.createClient();

module.exports = async () => {
    client.hgetall('manufacturerList', function (err, reply) {
        return data = JSON.parse(reply.data);
    });

    return data;
};