const redis = require('redis');
const client = redis.createClient();
const getManufacturerListFromApi = require("./getManufacturerList");

module.exports = async () => {
    try {
        let response = await getManufacturerListFromApi();

        let filteredData = response.data.map(function (item) {
            return { manufacturerId: item.manufacturerId, manufacturerName: item.manufacturerName };
        });
        client.hset('manufacturerList', 'data', JSON.stringify(filteredData));

    } catch (error) {
        console.error(error);
    }
}