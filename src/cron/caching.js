const cron = require('node-cron');
const storeManufacturerData = require("../services/storeConfigurationData")
const storeConfigurationData = require("../services/storeConfigurationData")

module.exports = async () => {
    cron.schedule('0 0 * * *', function () {
        storeManufacturerData();
        storeConfigurationData();
    });
}