const config = {};

config.web = {};
config.fake_data = {};

config.price_digest_api_key = process.env.PRICE_DIGEST_API_KEY;

config.web.port = 3000;

config.fake_data.year = 2000;
config.fake_data.manufacturerId = 693;
config.fake_data.classificationId = 1;

module.exports = config;
